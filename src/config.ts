export const DOMAIN = 'nvpunk.gabmus.org';
export const BASE_URL = `https://${DOMAIN}/`;
export const SITE_NAME = 'Nvpunk';
export const LOGO = '/img/nvpunk.svg';
export const BLURB = 'Neovim distribution for netrunners, cyberspace hackers and interstellar cowboys';
export const AUTHOR = 'Gabriele Musco';
export const YEAR = new Date().getFullYear();
export const GIT_REPO = 'https://gitlab.com/gabmus/nvpunk';
export const SITE_GIT_REPO = 'https://gitlab.com/gabmus/nvpunk.gabmus.org';
export const SCREENSHOTS = [
    '/img/screenshots/1.png',
    '/img/screenshots/2.png',
    '/img/screenshots/3.png',
    '/img/screenshots/4.png',
    '/img/screenshots/5.png',
    '/img/screenshots/6.png',
    '/img/screenshots/7.png',
    '/img/screenshots/8.png',
    '/img/screenshots/9.png',
    '/img/screenshots/10.png',
    '/img/screenshots/11.png',
    '/img/screenshots/12.png',
    '/img/screenshots/13.png',
    '/img/screenshots/14.png',
    '/img/screenshots/15.png',
];
export const FEAT_IMG = '/img/screenshots/feat.png'
