---
layout: ../../layouts/DocsLayout.astro
title: Configure
---
import CodeblockWrapper from '../../components/CodeblockWrapper.astro';

# Configuring Nvpunk

## Preferences

Nvpunk includes a preferences menu to help you customize it to your liking.

This menu can be accessed either from the *Preferences* button in the greeter, or by running the following command:

<CodeblockWrapper>
```vim
:NvpunkPreferences
```
</CodeblockWrapper>

From it, you will be able to:

- choose a new theme from a curated selection of popular and well supported themes
- change the greeter header unicode image
- alter the interface
  - toggle indentation guides
  - toggle code breadcrumbs
  - change the visual style for tabs
  - change the visual style for the status bar
  - change the borders of big and small floating windows, as well as menu popups (only partially supported)

More preferences will be added in the future to offer more high level controls for configuring Nvpunk to your liking.

## User Configuration

You can add your own lua configuration to Nvpunk. This is for advanced users that know how to configure Neovim and want to use Nvpunk as a base.

Inside your `$XDG_CONFIG_HOME` (by default it is `~/.config`), create a directory called `nvpunk`, inside that another called `lua`, and inside that a final one called `user`. In one command:

<CodeblockWrapper>
```bash
mkdir -p ~/.config/nvpunk/lua/user
```
</CodeblockWrapper>

Inside the `user` directory you can add the following files:

- `init.lua`
- `plugins.lua`
- `themes.lua`

`init.lua` will be called after the Nvpunk configuration is loaded. You can load any configuration override or other custom configuration code in here.

`plugins.lua` needs to return a table of tables, each subtable is a <a href="https://github.com/folke/lazy.nvim" target="_blank">Lazy</a> style plugin definition. After adding your custom plugins to `plugins.lua`, you can install them by restarting nvim and running `:Lazy install`.

Here is an example of a `plugins.lua` file:

<CodeblockWrapper>
```lua
return {
    {'vimwiki/vimwiki'},
    {'someuser/someplugin', branch = 'main', requires = {
        'someother/plugin'
    }},
}
```
</CodeblockWrapper>

`themes.lua` needs to return a table, each element of the table can either be a string representing a colorscheme that can then be loaded as is, or a function with a key that will be run to apply the theme. This can be useful in case a particular user-provided theme (installed by adding it to `plugins.lua`) requires extra configuration. Here is an example of a `themes.lua` file:

<CodeblockWrapper>
```lua
return {
    'blue',
    'delek',
    my_custom_theme = function()
        require'my_custom_theme'.setup {
            italic_comments = true,
        }
        vim.cmd'colorscheme my_custom_theme'
    end,
    my_other_theme = function()
        local t = require'my_other_theme'
        t.setup {
            some_config_opt = 'foo',
        }
        t.apply()
    end,
}
```
</CodeblockWrapper>

### Snippets

You can add your own snippets in vscode style format.

Create `~/.config/nvpunk/snippets/package.json`, and define your snippet files
similarly to this:

<CodeblockWrapper>
```json
{
    "name": "nvpunk-user-snippets",
    "contributes": {
        "snippets": [
            {
                "language": [
                    "typescript"
                ],
                "path": "./typescript.json"
            }
        ]
    }
}
```
</CodeblockWrapper>

Then in the `~/.config/nvpunk/snippets` folder create the files you referenced in the package.json you just created, and add your vscode style snippets, here is an example `typescript.json`:

<CodeblockWrapper>
```json
{
    "My function template": {
        "prefix": ["mfn", "myfun"],
        "body": [
            "async function ${1:name}(opts: MyFunOpts): Promise<MyFunResult> {",
            "    ${0:body}",
            "}"
        ],
        "description": "Quickly write a function signature I use often"
    }
}
```
</CodeblockWrapper>

To define snippets for all languages use language "all".

For more information reference the [luasnip documentation](https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md#vs-code).

## Altering Nvpunk

Feel free to edit Nvpunk's configuration on your own to adapt it to your needs, or contribute directly to it by [submitting a merge request](https://gitlab.com/gabmus/nvpunk/-/merge_requests).
