---
layout: ../../layouts/DocsLayout.astro
title: Install
---
import CodeblockWrapper from '../../components/CodeblockWrapper.astro';
import Details from '../../components/Details.astro';

# Install

## Prerequisites

### Fonts

Nvpunk makes extensive use of icons. To be able to see these icons and get the best experience possible you will need to install a <a href="https://www.nerdfonts.com/" target="_blank">Nerd Font patched font</a>.

They offer a wide selection of very popular programming fonts, and your favourite may very well be in the list.

### Neovim

You will need  <a href="https://neovim.io/" target="_blank">Neovim</a> version 0.8+ to use Nvpunk. You can probably find it **in your distribution's repositories**, alternatively you will find an extensive list of other methods in <a href="https://github.com/neovim/neovim/wiki/Installing-Neovim#linux" target="_blank">Neovim's Wiki</a>.

<Details text=" Debian">
<CodeblockWrapper>
```bash
sudo apt install neovim
```
</CodeblockWrapper>
</Details>

<Details text=" Arch">
<CodeblockWrapper>
```bash
sudo pacman -S neovim
```
</CodeblockWrapper>
</Details>

<Details text=" Fedora">
<CodeblockWrapper>
```bash
sudo dnf install neovim
```
</CodeblockWrapper>
</Details>

<Details text=" Brew">
<CodeblockWrapper>
```bash
brew install neovim
```
</CodeblockWrapper>
</Details>

### Git

Git is a version control system, it's required to download Nvpunk's plugins.

<Details text=" Debian">
<CodeblockWrapper>
```bash
sudo apt install git
```
</CodeblockWrapper>
</Details>

<Details text=" Arch">
<CodeblockWrapper>
```bash
sudo pacman -S git
```
</CodeblockWrapper>
</Details>

<Details text=" Fedora">
<CodeblockWrapper>
```bash
sudo dnf install git
```
</CodeblockWrapper>
</Details>

<Details text=" Brew">
<CodeblockWrapper>
```bash
brew install git
```
</CodeblockWrapper>
</Details>

### C/C++ compiler

To build Tree-sitter modules you will also need a C/C++ compiler. You can usually get one by installing a specific package group on your distribution, here are some examples:

<Details text=" Debian">
<CodeblockWrapper>
```bash
sudo apt install build-essential
```
</CodeblockWrapper>
</Details>

<Details text=" Arch">
<CodeblockWrapper>
```bash
sudo pacman -S base-devel
```
</CodeblockWrapper>
</Details>

<Details text=" Fedora">
<CodeblockWrapper>
```bash
sudo dnf group install "C Development Tools and Libraries" "Development Tools"
```
</CodeblockWrapper>
</Details>

<Details text=" Brew">
<CodeblockWrapper>
```bash
brew install gcc
```
</CodeblockWrapper>
</Details>

### Npm

Npm is required to download and install many language servers that are in fact written in javascript.

This applies also for non-javascript languages, for example the python language server pyright is written in javascript and requires npm.

<Details text=" Debian">
<CodeblockWrapper>
```bash
sudo apt install npm
```
</CodeblockWrapper>
</Details>

<Details text=" Arch">
<CodeblockWrapper>
```bash
sudo pacman -S npm
```
</CodeblockWrapper>
</Details>

<Details text=" Fedora">
<CodeblockWrapper>
```bash
sudo dnf install npm
```
</CodeblockWrapper>
</Details>

<Details text=" Brew">
<CodeblockWrapper>
```bash
brew install node
```
</CodeblockWrapper>
</Details>

### Python

Python is required to run certain language servers.

Python should be preinstalled on most Linux distributions as well as mac os.

### Ripgrep

Ripgrep is a line-oriented search tool that recursively searches the current directory for a regex pattern. Ripgrep is used by the Telescope live grep functionality and it's therefore a requirement for Nvpunk.

<Details text=" Debian">
<CodeblockWrapper>
```bash
sudo apt install ripgrep
```
</CodeblockWrapper>
</Details>

<Details text=" Arch">
<CodeblockWrapper>
```bash
sudo pacman -S ripgrep
```
</CodeblockWrapper>
</Details>

<Details text=" Fedora">
<CodeblockWrapper>
```bash
sudo dnf install ripgrep
```
</CodeblockWrapper>
</Details>

<Details text=" Brew">
<CodeblockWrapper>
```bash
brew install ripgrep
```
</CodeblockWrapper>
</Details>

### Terminal Emulator

While not mandatory, a good terminal emulator with proper font rendering and good support for icon fonts will greatly improve your experience.

Here is a list of recommended terminal emulators for you to try:

- <a href="https://wezfurlong.org/wezterm/" target="_blank">Wezterm</a>
- <a href="https://sw.kovidgoyal.net/kitty/" target="_blank">Kitty</a>
- <a href="https://github.com/alacritty/alacritty" target="_blank">Alacritty</a>
- <a href="https://codeberg.org/dnkl/foot" target="_blank">Foot</a>

## Install Nvpunk

Nvpunk uses <a href="https://git-scm.com/" target="_blank">Git</a> to update itself.

First, if you have already used Neovim in the past, you will probably want to back up your current configuration:

<CodeblockWrapper>
```bash
mv ~/.config/nvim ~/.config/nvim.bak
```
</CodeblockWrapper>

Now you can clone the latest version of Nvpunk to your nvim configuration.

<CodeblockWrapper>
```bash
git clone https://gitlab.com/gabmus/nvpunk ~/.config/nvim
```
</CodeblockWrapper>

## Health check

Nvpunk includes a self health check system to ensure everything is running smoothly. To run a health check you can use the *Health Check* button in the greeter, or the following command:

<CodeblockWrapper>
```vim
:NvpunkHealthcheck
```
</CodeblockWrapper>

Selecting one of the healthcheck results and pressing enter will open up a help page explaining what the dependency is and why it's required.

## Updating Nvpunk

You can easily update Nvpunk by either using the *Update Nvpunk* button in the greeter, or with the following command:

<CodeblockWrapper>
```vim
:NvpunkUpdate
````
</CodeblockWrapper>
